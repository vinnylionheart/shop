﻿using Microsoft.AspNetCore.Mvc;
using Shop.Data.Interfaces;
using Shop.Data.Models;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class ItemController : Controller
    {
        int ider = 0;
        public int  Ider(int id)
        {
            ider = id;
            return ider;
        }

        private readonly IAllMerches _allMerches;
        private readonly IMerchesCategory _allCategories;

        public ItemController(IAllMerches iAllMerches, IMerchesCategory iMerchesCategory)
        {
            _allMerches = iAllMerches;
            _allCategories = iMerchesCategory;
        }


        [Route("Item/{named}")]

        

        public ViewResult Item(string named) //К какому листу обращается в папке
        {
            string _named = named;
            IEnumerable<Merch> merches = null;
            string currNamed = "";
            if (string.IsNullOrEmpty(named))
            {
                merches = _allMerches.Merches.OrderBy(i => i.Id);
            }
            else
            {
                if (string.Equals("1", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("1"));
                }
                else if (string.Equals("2", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("2"));
                }
                else if (string.Equals("3", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("3"));
                }
                else if (string.Equals("4", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("4"));
                }
                else if (string.Equals("5", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("5"));
                }
                else if (string.Equals("6", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("6"));
                }
                else if (string.Equals("7", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("7"));
                }
                else if (string.Equals("8", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("8"));
                }
                else if (string.Equals("9", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("9"));
                }
                else if (string.Equals("10", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("10"));
                }
                else if (string.Equals("11", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("11"));
                }
                else if (string.Equals("12", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("12"));
                }
                else if (string.Equals("13", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("13"));
                }
                else if (string.Equals("14", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("14"));
                }
                else if (string.Equals("15", named, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.url.Equals("15"));
                }
            }
            var merchObj = new ItemViewModel
            {
                allMerches = merches,
                currNamed = currNamed
            };
            ViewBag.Title = "BIG HAUSE";

            return View(merchObj);
        }
    }
}
