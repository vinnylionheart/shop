﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Models
{
    public class ShoppingCartItem
    {
        public int Id { get; set; }
        public Merch Merch  { get; set; }
        public int Price { get; set; }
        public string Img { get; set; }
        public string ShoppingCartId { get; set; }
        public string Size { get; set; }
    }
}
