﻿using Microsoft.EntityFrameworkCore;
using Shop.Data.Interfaces;
using Shop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Repository
{
    public class ShoppingCartItemRepository: IShoppingCartItem
    {
        private readonly AppDBContext appDBContent;

        public ShoppingCartItemRepository(AppDBContext appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<ShoppingCartItem> ShoppingCartItemes => appDBContent.ShoppingCartItems.Include(c => c);
    }
}
