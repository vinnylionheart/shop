﻿using Microsoft.EntityFrameworkCore;
using Shop.Data.Interfaces;
using Shop.Data.Models;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shop.Controllers;

namespace Shop.Data.Repository
{
    public class SearchRepository: ISearch
    {


        private readonly AppDBContext appDBContent;

        public SearchRepository(AppDBContext appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        



        public IEnumerable<Merch> GetSearchMerches => appDBContent.Merch.Include(c => c.Category);
    }
}
