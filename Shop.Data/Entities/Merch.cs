﻿using Shop.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.WebApp.Models
{
    public class Merch : IEntity
    {
        public long Id { set; get; }

        public string name { set; get; }
        public string description { set; get; }
        public string img { set; get; }
        public int price { set; get; }
        public bool isFavourite { set; get; }
        public int available { set; get; }
        public int categoryID { set; get; }

        public virtual Category Category { set; get; }
    }
}
