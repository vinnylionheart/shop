﻿using Shop.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels
{
    public class SearchFiltersViewModel
    {
        public IEnumerable<Merch> searchMerches { get; set; }
    }
}
