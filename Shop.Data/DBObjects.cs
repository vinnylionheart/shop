﻿using Shop.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop
{
    public class DBObjects
    {
        public static void Initial(AppDBContext content)
        {

            if (!content.Category.Any())
                content.Category.AddRange(Categories.Select(c => c.Value));

            if (!content.Merch.Any())
            {
                content.AddRange(
                    new Merch
                    {
                        name = "Футболка чёрная",
                        description = "Удобная ",
                        img = "/img/Tesla.jpg",
                        price = 45000,
                        isFavourite = true,
                        available = 1,
                        Category = Categories["Футболки"]
                    },
                    new Merch
                    {
                        name = "Футблока белая",
                        description = "Красивая",
                        img = "/img/ford.jpg",
                        price = 11000,
                        isFavourite = false,
                        available = 1,
                        Category = Categories["Футболки"]
                    },
                    new Merch
                    {
                        name = "Кепка зелёная",
                        description = "Зелёная",
                        img = "/img/bmw.jpg",
                        price = 65000,
                        isFavourite = true,
                        available = 1,
                        Category = Categories["Кепки"]
                    },
                    new Merch
                    {
                        name = "Браслет резиновый",
                        description = "Овальная",
                        img = "/img/mercedes.jpg",
                        price = 40000,
                        isFavourite = false,
                        available = 1,
                        Category = Categories["Браслеты"]
                    },
                    new Merch
                    {
                        name = "Толстовка с карманами",
                        description = "Тёплая",
                        img = "/img/nissan.jpg",
                        price = 14000,
                        isFavourite = true,
                        available = 1,
                        Category = Categories["Толстовки"]
                    }
                    );
            }

            content.SaveChanges();

        }
        private static Dictionary<string, Category> category;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (category == null)
                {
                    var list = new Category[]
                    {
                        new Category { categoryName = "Футблоки", description = "Классные футболки" },
                        new Category { categoryName = "Толстовки",  description= "Тёплые"},
                        new Category { categoryName = "Браслеты", description = "Да" },
                        new Category { categoryName = "Кепки",  description= "Нет"}
                    };

                    category = new Dictionary<string, Category>();
                    foreach (Category el in list)
                        category.Add(el.categoryName, el);
                }
                return category;
            }
        }
    }
}
