﻿using Microsoft.AspNetCore.Mvc;
using Shop.Data.Interfaces;
using Shop.Data.Models;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class MerchesController: Controller
    {
        private readonly IAllMerches _allMerches;
        private readonly IMerchesCategory _allCategories;

        public MerchesController (IAllMerches iAllMerches, IMerchesCategory iMerchesCategory)
        {
            _allMerches = iAllMerches;
            _allCategories = iMerchesCategory;
        }

        [Route("Merches/List")]
        [Route("Merches/List/{category}")]
        public ViewResult List(string category)
        {
            string _category = category;
            IEnumerable<Merch> merches = null;
            string currCategory = "";
            if (string.IsNullOrEmpty(category))
            {
                merches = _allMerches.Merches.OrderBy(i => i.Id);
            }
            else
            {
                if (string.Equals("t-shirts", category, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.Category.CategoryName.Equals("Футболка")).OrderBy(i => 1);
                    currCategory = "Футболки";
                }
                else if (string.Equals("sweatshirts", category, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.Category.CategoryName.Equals("Толстовка")).OrderBy(i => 5);
                    currCategory = "Толстовки";
                }
                else if (string.Equals("caps", category, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.Category.CategoryName.Equals("Кепка")).OrderBy(i => i.Id);
                    currCategory = "Кепки";
                }
                else if (string.Equals("bracelets", category, StringComparison.OrdinalIgnoreCase))
                {
                    merches = _allMerches.Merches.Where(i => i.Category.CategoryName.Equals("Браслет")).OrderBy(i => i.Id);
                    currCategory = "Браслеты";
                }
            }
            var merchObj = new MerchListViewModel
            {
                allMerches = merches,
                currCategory = currCategory
            };
            ViewBag.Title = "BIG HAUSE";

            return View(merchObj);
        }
    }
    
}
