﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shop.Data.Interfaces;
using Shop.ViewModels;

namespace Shop.Controllers
{
    public class HomeController : Controller
    {
        private IAllMerches _carRep;


        public HomeController(IAllMerches carRep)
        {
            _carRep = carRep;
        }
        
        public ViewResult Index()
        {
            var homeMerches= new HomeViewModels
            {
                favMerches = _carRep.GetFavMerches
            };
            return View(homeMerches);
        }

    }
}
