﻿using Shop.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.WebApp.Models
{
    public class Category : IEntity
    {
        public long Id { get; set; }

        public string categoryName { set; get; }

        public string description { set; get; }

        public List<Merch> merch { set; get; }
    }
}
