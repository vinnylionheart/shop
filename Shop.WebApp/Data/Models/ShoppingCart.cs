﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Models
{
    public class ShoppingCart
    {
        private readonly AppDBContext appDBContext;

        public ShoppingCart(AppDBContext appDBContext)
        {
            this.appDBContext = appDBContext;
        }
        public string ShoppingCartId { get; set; }

        public List<ShoppingCartItem> listShopItems { get; set; }

        public static ShoppingCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            var contex = services.GetService<AppDBContext>();
            string shoppingCartId = session.GetString("CartId") ?? Guid.NewGuid().ToString();

            session.SetString("CartId", shoppingCartId);

            return new ShoppingCart(contex) { ShoppingCartId = shoppingCartId };
        }

        public void AddToCart(Merch merch, string size)
        {
            appDBContext.ShoppingCartItems.Add(new ShoppingCartItem
            {
                ShoppingCartId = ShoppingCartId,
                Merch = merch,
                Price = merch.Price,
                Img = merch.Img,
                Size = size

            }); ;

            appDBContext.SaveChanges();
        }

        public void DelToCart(ShoppingCartItem shoppingCartItem)
        {
            appDBContext.ShoppingCartItems.Remove(shoppingCartItem);
            appDBContext.SaveChanges();
        }


        public List<ShoppingCartItem> getShopItems()
        {
            return appDBContext.ShoppingCartItems.Where(c => c.ShoppingCartId == ShoppingCartId).Include(s => s.Merch).ToList();
        }

    }
}
