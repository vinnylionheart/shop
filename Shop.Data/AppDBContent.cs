﻿using Microsoft.EntityFrameworkCore;
using Shop.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop
{
    public class AppDBContext: DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        public DbSet<Merch> Merch { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}
