﻿using Microsoft.AspNetCore.Mvc;
using Shop.Data.Interfaces;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class SearchController : Controller
    {

        private ISearch _merchRep;

        public SearchController(ISearch merchRep)
        {
            _merchRep = merchRep;
        }

        [HttpPost]
        public ViewResult Index(string search)
        {
                var searchMerches = new SearchFiltersViewModel
                {
                    searchMerches = _merchRep.GetSearchMerches.Where(x => x.Name.Contains(search))
                };

            return View(searchMerches);
        }
    }
}
