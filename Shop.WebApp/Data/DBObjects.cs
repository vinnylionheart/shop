﻿
using Shop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data
{
    public class DBObjects
    {
        public static void Starter (AppDBContext content)
        {

            if (!content.Categories.Any())
                content.Categories.AddRange(Categories.Select(c => c.Value));

            if (!content.Merch.Any())
            {
                content.AddRange(
                    new Merch
                    {
                        Name = "Толстовка Анимэ",
                        Description = "Уход за вещамибережная стирка при 30 градусах Декоративные элементыбез элементов " +
                        "Покройсвободный Длина изделия по спинке64 см Комплектацияфутболка " +
                        "ПолМужской Страна брендаРоссия Страна производительУзбекистан",
                        Img = "/img/blue_anime.jpg",
                        Price = 1999,
                        IsFavourite = true,
                        Available = 1,
                        url = "1",
                        Category = Categories["Толстовка"]
                    },
                    new Merch
                    {
                        Name = "Футболка Белая",
                        Description = "Уход за вещамибережная стирка при 30 градусах Декоративные элементыбез элементов " +
                        "Покройсвободный Длина изделия по спинке64 см Комплектацияфутболка " +
                        "ПолМужской Страна брендаРоссия Страна производительУзбекистан",
                        Img = "/img/f1.jpg",
                        Price = 999,
                        IsFavourite = false,
                        Available = 1,
                        url = "2",
                        Category = Categories["Футболка"]
                    },
                    new Merch
                    {
                        Name = "Футболка Белая",
                        Description = "Уход за вещамибережная стирка при 30 градусах Декоративные элементыбез элементов " +
                        "Покройсвободный Длина изделия по спинке64 см Комплектацияфутболка " +
                        "ПолМужской Страна брендаРоссия Страна производительУзбекистан",
                        Img = "/img/f2.jpg",
                        Price = 999,
                        IsFavourite = true,
                        Available = 1,
                        url = "3",
                        Category = Categories["Футболка"]
                    }
                    );
            }

            content.SaveChanges();

        }
        private static Dictionary<string, Category> category;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (category == null)
                {
                    var list = new Category[]
                    {
                        new Category { CategoryName = "Футболка", Description = "Классные футболки" },
                        new Category { CategoryName = "Толстовка",  Description= "Тёплые"},
                        new Category { CategoryName = "Браслет", Description = "Да" },
                        new Category { CategoryName = "Кепка",  Description= "Нет"}
                    };

                    category = new Dictionary<string, Category>();
                    foreach (Category el in list)
                        category.Add(el.CategoryName, el);
                }
                return category;
            }
        }
    }
}
