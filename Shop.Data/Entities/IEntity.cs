﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Data.Entities
{
    public interface IEntity
    {
        public long Id { get; set; }
    }
}
