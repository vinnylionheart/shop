﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shop.Data.Interfaces;
using Shop.Data.Models;

namespace Shop.Data.Repository
{
    public class MerchRepository : IAllMerches
    {
        private readonly AppDBContext appDBContent;

        public MerchRepository(AppDBContext appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public IEnumerable<Merch> Merches => appDBContent.Merch.Include(c => c.Category);

        public IEnumerable<Merch> GetFavMerches => appDBContent.Merch.Where(p => p.IsFavourite).Include(c => c.Category);

        public Merch getObjectMerch(int Id) => appDBContent.Merch.FirstOrDefault(p => p.Id == Id);


    }
}

