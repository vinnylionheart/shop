﻿using Shop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels
{
    public class ItemViewModel
    {

        public IEnumerable<Merch> allMerches { get; set; }

        public string currNamed { get; set; }
    }
}