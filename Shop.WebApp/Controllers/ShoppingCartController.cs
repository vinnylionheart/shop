﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shop.Data.Interfaces;
using Shop.Data.Models;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class ShoppingCartController: Controller
    {
        private IAllMerches _merchRep;
        private IShoppingCartItem _shp;
        private readonly ShoppingCart _shoppingCart;


        public ShoppingCartController(IAllMerches merchRep, ShoppingCart shoppingCart, IShoppingCartItem shp)
        {
            _merchRep = merchRep;
            _shoppingCart = shoppingCart;
            _shp = shp;
        }

        public ViewResult Index()
        {   
            
            var Items = _shoppingCart.getShopItems();
            _shoppingCart.listShopItems = Items;

            var obj = new ShoppingCartViewModel
            {
                shoppingCart = _shoppingCart
            };

            return View(obj);
        }
        
        [Authorize]
        public RedirectToActionResult addToCart(int id, string size)
        {
            var item = _merchRep.Merches.FirstOrDefault(i => i.Id == id);
            if (item != null)
            {
                _shoppingCart.AddToCart(item,size);
            }
            return RedirectToAction("Index");
        }


        public RedirectToActionResult delToCart(int id)
        {
            var item = _shp.ShoppingCartItemes.FirstOrDefault(i => i.Id == id);
            if (item != null)
            {
                _shoppingCart.DelToCart(item);
            }
            return RedirectToAction("Index");
        }

        public int Sum(string shoppingCartId)
        {
            var sum = _shp.ShoppingCartItemes.Where(i => i.ShoppingCartId == shoppingCartId).Sum(i=>i.Price);
                return sum;
        }


    }
}
