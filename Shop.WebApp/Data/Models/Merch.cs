﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Models
{
    public class Merch 
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public string Img { set; get; }
        public int Price { set; get; }
        public bool IsFavourite { set; get; }
        public int Available { set; get; }
        public int CategoryID { set; get; }
        public string url { set; get; }
        public string size { set; get; }

        public virtual Category Category { set; get; }
    }
}
